﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using MachineActivate.Data;

namespace MachineActivate
{
    public partial class GrabData : Form
    {
        private readonly MainForm _mainForm;

        public GrabData(MainForm mainForm)
        {
            _mainForm = mainForm;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Task<DataTable>.Factory.StartNew(() =>
            {
                return Database.Instance.GetComments();
            })
                .ContinueWith(task =>
                {
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            _mainForm.Show();
        }

        private void waitPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://vk.com/topic-89068792_32522802");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://rayhack.ru");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://vk.com/hz_team");
        }
    }
}
