﻿using System.Management;

namespace MachineActivate
{
    public static class HWID
    {
        public static string GetHWID()
        {
            var mbs = new ManagementObjectSearcher("Select * from Win32_processor");
            ManagementObjectCollection mbslist = mbs.Get();
            string r = "";
            foreach(ManagementObject mo in mbslist)
            {
                r = mo["ProcessorID"].ToString();
            }

            return r;
        }
    }
}
