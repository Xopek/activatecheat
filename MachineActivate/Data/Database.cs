﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Threading;
using MySql.Data.MySqlClient;

namespace MachineActivate.Data
{
    internal partial class Database
    {
        private static Database _instance;
        private static readonly object _sync = new object();
        protected string _connectionString;

        public Database()
        {
        }

        /// <summary>
        /// Создаёт единственный экземпляр объекта в памяти. Свойство потокобезопасно.
        /// </summary>
        public static Database Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new Database();
                        }
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Установить строку подключения для базы данных
        /// </summary>
        /// <param name="user">Имя пользователя БД</param>
        /// <param name="password">Пароль рользователя БД</param>
        /// <param name="database">Имя БД</param>
        /// <param name="server">Адрес сервера БД</param>
        /// <param name="port">Порт сервера БД</param>
        public void SetConnectionString(string user, string password, string database, string server, uint port = 3306)
        {
            var mySqlConnectionStringBuilder = new MySqlConnectionStringBuilder();
            mySqlConnectionStringBuilder.UserID = user;
            mySqlConnectionStringBuilder.Password = password;
            mySqlConnectionStringBuilder.Server = server;
            mySqlConnectionStringBuilder.Database = database;
            mySqlConnectionStringBuilder.Port = port;
            mySqlConnectionStringBuilder.MaximumPoolSize = 50;
            mySqlConnectionStringBuilder.MinimumPoolSize = 2;
            mySqlConnectionStringBuilder.Pooling = true; // Важно!!!

            _connectionString = mySqlConnectionStringBuilder.ToString();
        }

        /// <summary>
        /// Проверить соединение с базой данных.
        /// </summary>
        /// <returns></returns>
        public bool CheckConnection()
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("CheckConnection. При подключении к БД вылетело исключение: " + ex.Message);
                    return false;
                }
            }
        }

        #region Общие методы для всех по работе с базой данных (редактирование, получение каких-то общих данных)

        /// <summary>
        /// Получить кол-во записей из другого запроса, вызывает внутри себя: 
        /// command.CommandText = string.Format("SELECT Count(*) FROM ( {0} ) c;", from);
        /// </summary>
        /// <param name="from">Запрос, из результата которого нужно получить кол-во записей в нём.</param>
        /// <param name="parameters">Параметры необходимые для выполнения переданного запроса.</param>
        /// <returns></returns>
        protected int GetCount(string from, MySqlParameter[] parameters)
        {
            var command = new MySqlCommand();
            command.Parameters.AddRange(parameters);
            command.CommandText = String.Format("SELECT Count(*) FROM ( {0} ) c;", from);

            using (command)
            using (var connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    return Int32.Parse(command.ExecuteScalar() + String.Empty);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("GetData. При получении данных из БД вылетело исключение: " + ex.Message);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Получить данные из базы данных в виде списка записей DbDataRecord. Для запросов SELECT. Вызывает внутри себя command.ExecuteReader()
        /// </summary>
        /// <param name="command">Команда</param>
        /// <returns></returns>
        protected List<DbDataRecord> GetData(MySqlCommand command)
        {
            var dataList = new List<DbDataRecord>();
            using (command)
            using (var connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    using (MySqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            foreach (DbDataRecord record in dataReader)
                            {
                                dataList.Add(record);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("GetData. При получении данных из БД вылетело исключение: " + ex.Message);
                }
            }
            return dataList;
        }

        protected object ExecuteQuery(MySqlCommand command)
        {
            using (command)
            using (var connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    return command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ExecuteQuery. Выполнении команды вылетело исключение: " + ex.Message);
                    return null;
                }
            }
        }


        /// <summary>
        /// Получить данные из базы данных в виде DataTable. Для запросов SELECT. Использует MySqlDataAdapter
        /// </summary>
        /// <param name="command">Команда</param>
        /// <returns></returns>
        protected DataTable GetDataTable(MySqlCommand command)
        {
            var answer = new DataTable();
            using (command)
            using (var connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    var da = new MySqlDataAdapter(command);
                    da.Fill(answer);
                    da.Dispose();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("GetDataTable. При чтении из БД произошло исключение: " + ex.Message);
                }
            }
            return answer;
        }

        /// <summary>
        /// Позволяет изменить данные в базе данных, например, запросы INSERT INTO, UPDATE, DELETE.
        /// </summary>
        /// <param name="command">Команда</param>
        /// <returns>Количество изменных строк</returns>
        protected int ChangeData(MySqlCommand command)
        {
            int changedRowsCount = 0;
            using (var connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    changedRowsCount = command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ChangeData. Изменении данных в БД: " + ex.Message);
                }
            }
            return changedRowsCount;
        }

        #endregion
    }
}
