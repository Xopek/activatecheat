﻿using System.Data;
using MySql.Data.MySqlClient;

namespace MachineActivate.Data
{
    internal partial class Database
    {
        public bool InsertActivationData(string userName, string sysKey)
        {
            const string commandText = "INSERT INTO activation (login, syskey)" +
                                       "VALUES (@UserName, @SysKey)";
            MySqlCommand command = new MySqlCommand(commandText);
            command.Parameters.AddWithValue("@UserName", userName);
            command.Parameters.AddWithValue("@SysKey", sysKey);
            return ChangeData(command) > 0;
        }

        public DataTable GetComments()
        {
            const string commandText = @"SELECT * FROM activation;";
            MySqlCommand command = new MySqlCommand(commandText);
            return GetDataTable(command);
        }

        public bool CheckAccess(string userName, string sysKey)
        {
            var command = new MySqlCommand();
            command.Parameters.Add(new MySqlParameter("@UserName", MySqlDbType.String) { Value = userName });
            command.Parameters.Add(new MySqlParameter("@SysKey", MySqlDbType.String) { Value = sysKey });
            command.CommandText =
                "SELECT * FROM activation act WHERE act.login=@UserName AND act.syskey=@SysKey AND act.activ=b'1'";

            return GetData(command).Count > 0;
        }
    }
}
