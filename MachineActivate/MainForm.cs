﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Management;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MachineActivate.Data;
using MachineActivate.Security;

namespace MachineActivate
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Database.Instance.SetConnectionString("sql580492", "bH7!lN8*", "sql580492", "sql5.freesqldatabase.com");

            try
            {
                string drive = Environment.GetFolderPath(Environment.SpecialFolder.System).Substring(0, 1);
                ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + drive + ":\"");
                disk.Get();
                string diskLetter = (disk["VolumeSerialNumber"].ToString());
                string uncrypted = (CryptHelper.Crypt(diskLetter));
                sysKeyTb.Text = uncrypted + HWID.GetHWID();
            }
            catch (Exception)
            {
                sysKeyTb.Text = "Ошибка при генерации системного ключа!";
            }
        }

        /// <summary>
        /// Переключает значение свойства Enabled. Можно вызывать из не UI потоков.
        /// </summary>
        /// <param name="enabled">Новое состояние</param>
        private void SetPanelState(bool enabled)
        {
            if (panel.InvokeRequired)
            {
                panel.Invoke((Action<bool>)SetPanelState, enabled);
            }
            else
            {
                panel.Enabled = enabled;
            }
        }

        private bool ValidateTextValue(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return false;
            }

            return true;
        }

        private void UserNameTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            string c = e.KeyChar.ToString();

            if (e.KeyChar != '\b' && !Regex.Match(c, @"[a-zA-Z]|[0-9]").Success)
            {
                e.Handled = true;
            }
        }

        private async void UpdateButton_Click(object sender, EventArgs e)
        {
            //проверяем, что текстовые поля содержат данные
            if (!(ValidateTextValue(userNameTextBox.Text) && ValidateTextValue(sysKeyTb.Text)))
            {
                MessageBox.Show(this, "Введите все данные");
                return;
            }
            SetPanelState(false);
            //в другом потоке данные отправятся и только после того, как данные 
            //будут обработаны метод продолжит работу, при этом интерфейс в этой части кода не будет зависать
            if (await InsertData(userNameTextBox.Text, sysKeyTb.Text))
            {
                MessageBox.Show(this, string.Format("Пользователь {0} зарегистрирован.", userNameTextBox.Text));
            }
            SetPanelState(true);
        }

        private Task<bool> InsertData(string userName, string sysKey)
        {
            //Запускаем в другом потоке (выполнение передаётся в пул потоков, новый поток тут не создаётся).
            return Task<bool>.Factory.StartNew(() =>
            {
                //Если удачно добавит, вернёт true
                return Database.Instance.InsertActivationData(userName, sysKey);
            });
        }

        private async void AuthButton_Click(object sender, EventArgs e)
        {
            //проверяем, что текстовые поля содержат данные
            if (!(ValidateTextValue(userNameTextBox.Text) && ValidateTextValue(sysKeyTb.Text)))
            {
                MessageBox.Show(this, "Введите все данные");
                return;
            }

            SetPanelState(false);
            var userExists = await Task<bool>.Factory.StartNew(() =>
               {
                   return Database.Instance.CheckAccess(userNameTextBox.Text, sysKeyTb.Text);
               });

            SetPanelState(true);
            if (userExists)
            {
                var grabData = new GrabData(this);
                grabData.Show();
            }
            else
            {
                MessageBox.Show(this, string.Format("Пользователь {0} не зарегистрирован или был зарегистрирован под другим ключом.", userNameTextBox.Text), "Внимание");
            }
        }

        private void InfoButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this,
                "Версия программы V1.3.5."+Environment.NewLine+
            "Разработана командой HackZone-Team специально для RayHack.ru","Информация!", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void AboutProgram()
        {
            MessageBox.Show(this, "Разработчики: CasperSC, HackZone, Guru, NerQ, 1DEFAULT1\n Специально для rayhack.ru", "Разработка",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ExitMeniStrip_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AboutMeniStrip_Click(object sender, EventArgs e)
        {
            AboutProgram();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}