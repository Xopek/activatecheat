﻿using System;
using System.Linq;
using MachineActivate.Security;

namespace Utils.Security.App
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string user = "sql580492";
            const string password = "bH7!lN8*";
            const string dbName = "sql580492";
            const string server = "sql5.freesqldatabase.com";

            string key = "jkcuuhh@#**J#@@HHSH!@*WE*".Replace('k', 'f')
                .Replace('@', '_')
                .Reverse().Select(ch => ch.ToString())
                .Aggregate((res, ch) =>
                {
                    res += ch;
                    return res;
                });

            string encryptedUser = Cryptor.EncryptTextTo3DES(user, key);
            string encryptedPassword = Cryptor.EncryptTextTo3DES(password, key);
            string encryptedDbName = Cryptor.EncryptTextTo3DES(dbName, key);
            string encryptedServer = Cryptor.EncryptTextTo3DES(server, key);

            string decryptedUser = Cryptor.DecryptTextFrom3DES(encryptedUser, key);
            string decryptedPassword = Cryptor.DecryptTextFrom3DES(encryptedPassword, key);
            string decryptedDbName = Cryptor.DecryptTextFrom3DES(encryptedDbName, key);
            string decryptedUServer = Cryptor.DecryptTextFrom3DES(encryptedServer, key);

            string line =
                string.Format("Database.Instance.SetConnectionString(\"{0}\", \"{1}\", \"{2}\", \"{3}\");",
                    encryptedUser, encryptedPassword, encryptedDbName, encryptedServer);

            Console.WriteLine(line);
            Console.ReadKey();
        }
    }
}